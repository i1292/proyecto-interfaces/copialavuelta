<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Datos';

?>

<div class="site-contact">
    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Maillots</h1>
    </div>
    <div class="body-content">
        <div class="row">
            <!-- Tarjeta 1 -->
            <div class="col-sm-6 col-md-4" style="padding-bottom: 15px">
                <div class="card" style="align-items: center">
                    
                    <?= Html::img("@web/images/foto.png",['class'=>"card-img-top"])?>
                    <p>Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8</p>
                    <p>
                        <?= Html::a('Editar',['site/ciclistasyequipos'],['class'=>'btn btn-primary'])?>
                        <?= Html::a('Eliminar',['ciclista/index'],['class'=>'btn btn-warning'])?>
                    </p>
                    
                </div>
            </div>
            <!-- Tarjeta 2 -->
            <div class="col-sm-6 col-md-4">
                <div class="card">
                    
                    <?= Html::img("@web/images/foto.png",['class'=>"card-img-top"])?>
                    <p>Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8</p>
                    <p>
                        <?= Html::a('Active Record',['site/consulta6a'],['class'=>'btn btn-primary'])?>
                        <?= Html::a('DAO',['site/consulta6'],['class'=>'btn btn-warning'])?>
                    </p>
                    
                </div>
            </div>
            <!-- Tarjeta 3 -->
            <div class="col-sm-6 col-md-4">
                <div class="card">
                    
                    <?= Html::img("@web/images/foto.png",['class'=>"card-img-top"])?>
                    <p>Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8</p>
                    <p>
                        <?= Html::a('Active Record',['site/consulta6a'],['class'=>'btn btn-primary'])?>
                        <?= Html::a('DAO',['site/consulta6'],['class'=>'btn btn-warning'])?>
                    </p>
                    
                </div>
            </div>
            <!-- Tarjeta 4 -->
            <div class="col-sm-6 col-md-4">
                <div class="card">
                    
                    <?= Html::img("@web/images/foto.png",['class'=>"card-img-top"])?>
                    <p>Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8</p>
                    <p>
                        <?= Html::a('Active Record',['site/consulta6a'],['class'=>'btn btn-primary'])?>
                        <?= Html::a('DAO',['site/consulta6'],['class'=>'btn btn-warning'])?>
                    </p>
                    
                </div>
            </div>
            <!-- Tarjeta 5 -->
            <div class="col-sm-6 col-md-4">
                <div class="card ">
                    
                    <?= Html::img("@web/images/foto.png",['class'=>"card-img-top"])?>
                    <p>Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8</p>
                    <p>
                        <?= Html::a('Active Record',['site/consulta6a'],['class'=>'btn btn-primary'])?>
                        <?= Html::a('DAO',['site/consulta6'],['class'=>'btn btn-warning'])?>
                    </p>
                    
                </div>
            </div>
            <!-- Tarjeta 6 -->
            <div class="col-sm-6 col-md-4">
                <div class="card">
                    
                    <?= Html::img("@web/images/foto.png",['class'=>"card-img-top"])?>
                    <p>Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8</p>
                    <p>
                        <?= Html::a('Active Record',['site/consulta6a'],['class'=>'btn btn-primary'])?>
                        <?= Html::a('DAO',['site/consulta6'],['class'=>'btn btn-warning'])?>
                    </p>
                    
                </div>
            </div>
        </div>
    </div>
</div>