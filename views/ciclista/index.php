<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ciclistas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ciclista-index">

    <h1 style="color: red"><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Nuevo Ciclista', ['create'], ['class' => 'btn btn-secondary']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'dorsal',
            'nombre',
            'edad',
            'nomequipo',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'dorsal' => $model->dorsal]);
                 }
            ],
        ],
    ]); ?>


</div>
