<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
$this->title = 'Datos generales';

?>
<div class="container">
    
    <div class="body-content">
        <div class="jumbotron text-center bg-transparent">
            <h1 class="display-4" style="padding-top: 15px">Información</h1>
        </div>
        <div class="row">
            
            <div class="col-sm-6 col-md-4 "> 
                <div class="card alturaminima shadow-sm">
                    
                    <div class="card-body tarjeta">
                        
                        <h3>Ciclistas</h3>
                        <p>Todos los ciclistas inscritos</p>
                           
                            <?= Html::a(Html::img('@web/images/ciclista.jpg',['width'=>'278','height'=>'150']),['/ciclista/index'],['class'=> 'imageneshome'])?>
                       
                    </div>
                </div>
             </div>  
            
             <div class="col-sm-6 col-md-4"> 
                <div class="card alturaminima shadow-sm">
                    <div class="card-body tarjeta">
                        <h3>Equipos</h3>    
                        <p>Equipos han participado</p>
                        
                            <?= Html::a(Html::img('@web/images/equipo.jpg',['width'=>'278','height'=>'150']),['/equipo/index'],['class'=> 'imageneshome'])?>
                        
                    </div>
                </div>
             </div>  
            
            <div class="col-sm-6 col-md-4"> 
                <div class="card alturaminima shadow-sm">
                    <div class="card-body tarjeta">
                        <h3>Etapas</h3>
                        <p>Todas las etapas que hay</p>
                        
                        <?= Html::a(Html::img('@web/images/etapa.jpg',['width'=>'278','height'=>'150']),['/etapa/index'],['class'=> 'imageneshome']) ?>
                        
                    </div>
                </div>
             </div>   
        
         
            <div class="col-sm-6 col-md-4"> 
                <div class="card alturaminima shadow-sm">
                    
                    <div class="card-body tarjeta">
                        
                        <h3>Maillots</h3>
                        <p>Todas las Maillots que hay</p>
                        
                         <?= Html::a(Html::img('@web/images/maillots.jfif',['width'=>'278','height'=>'150']),['/maillot/index'],['class'=> 'imageneshome'])?>
                        
                    </div>
                </div>
             </div>  
            
             <div class="col-sm-6 col-md-4"> 
                <div class="card alturaminima shadow-sm">
                    <div class="card-body tarjeta">
                        <h3>Puertos</h3>    
                        <p>Puertos que hay</p>
                        
                         <?= Html::a(Html::img('@web/images/puerto.jpg',['width'=>'278','height'=>'150']),['/puerto/index'],['class'=> 'imageneshome'])?>
                        
                    </div>
                </div>
             </div>  
            
            <div class="col-sm-6 col-md-4"> 
                <div class="card alturaminima shadow-sm">
                    <div class="card-body tarjeta">
                        <h3>LLevados</h3>
                        <p>Dorsal que ha llevado el ciclista</p>
                        
                         <?= Html::a(Html::img('@web/images/dorsalllevado.jpg',['width'=>'278','height'=>'150']),['/lleva/index'],['class'=> 'imageneshome'])?>
                        
                    </div>
                </div>
             </div> 
            </div>
    </div>
    </div> 
