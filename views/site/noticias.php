<?php
use yii\helpers\Html;
$this->title = 'Noticias';
?>


<div class="container">
    <div class="row stilobody">
        <div class="align-items-left col-12 card shadow-sm" style="padding: 30px; margin-bottom: 30px">
            <h1>Gorospe cedio el liderato al italiano Giovannetti</h1>
            <hr>
            <p class="text-justify">Un aspirante al mero triunfo de etapa, Carlos Hernández (Lotus), volvió a demostrar ayer que los favoritos de la Vuelta continúan todavía lejos de imponer su autoridad sobre la carrera. Hernández se adjudicó la victoria en una jornada, la 11ª entre León y la Estación de San Isidro, de 203 kilómetros, en la que los grandes se limitaron a rodar fuerte. Sólo los que no lograron mantener el ritmo cedieron tiempo, entre ellos el líder, Julián Gorospe (Banesto), que cedió su jersei amarillo al italiano Marco Giovannetti (Seur), un buen corredor que fue sexto en un Giro, pero cuyas perspectivas para mantenerlo son escasas.</p>
            <?= Html::a('Seguir leyendo...',['site/noticia'])?>
        </div>
        <div class="align-items-left col-12 card shadow-sm" style="padding: 30px; margin-bottom: 30px">
            <h1>Los directores de equipos asumen el riesgo de sus tácticas</h1>
            <hr>
            <p class="text-justify">Los directores deportivos de los equipos españoles asumen el notable riesgo del peligroso juego táctico que han utilizado en las seis primeras etapas de la Vuelta. El resultado es que los principales favoritos han cedido un tiempo importante respecto a corredores que, sin ser jefes de fila, pueden dar motivos a múltiples especulaciones sobre su opción para mantener sus actuales posiciones. José Miguel Echávarri (Banesto) explica su criterio sobre lo que ha sucedido hasta el momento Refiriéndose a Gorospe, Giovanetti, Cadena, Ivanov y Farfan, explica: "Hasta ahora esta mos poniendo sobre el tapete caballos y sotas y nos reservamos los ases y los reyes. Pero es un juego muy peligroso. Nadie puede estar tranquilo con esta situación, aunque nuestra posición es mejor que la de otros".Existe coincidencia unánime en definir como una "locura" el desarrollo de la carrera.</p>
            <?= Html::a('Seguir leyendo...',['site/noticia2'])?>
        </div>
        <div class="align-items-left col-12 card shadow-sm" style="padding: 30px; margin-bottom: 30px">
            <h1>Primer golpe de mano de Banesto en la Vuelta</h1>
            <hr>
            <p class="text-justify">La omnipresencia parece ser la regla de (oro para todo aspirante a ganar la actual edición de la Vuelta a España. Desaparecer de la cabeza del pelotón, tan siquiera fugazmente, conduce directamente al furgón de cola. Ayer lo comprobaron Lejarreta y Cabestany, Pino, y el suizo Rominger, que perdieron 52 segundos respecto al grupo de los mejores, que voló bajo la impulsión del equipo Banesto de Delgado e Induráin.</p>
            <?= Html::a('Seguir leyendo...',['site/noticia3'])?>
        </div>
    </div>
</div>